void drawBoot(OLEDDisplay *display, String label, String message, int counter) {
  display->clear();
  display->drawString(64, 10, label);
  display->drawString(64, 50, message);
  display->drawXbm(46, 30, 8, 8, counter % 3 == 0 ? activeSymbole : inactiveSymbole);
  display->drawXbm(60, 30, 8, 8, counter % 3 == 1 ? activeSymbole : inactiveSymbole);
  display->drawXbm(74, 30, 8, 8, counter % 3 == 2 ? activeSymbole : inactiveSymbole);
  display->display();
}

void drawProgress(OLEDDisplay *display, int percentage, String label, String message) {
  display->clear();
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(64, 5, label);
  display->drawProgressBar(2, 28, 124, 10, percentage);
  display->drawString(64, 50, message);
  display->display();
}

void scanI2C() {
  int counter = 0;
  byte count = 0;
  Serial.println("Début du scan I2C...");
  if (oled_display) { 
    drawBoot(&display, "I2C", "Début du scan I2C", counter);
    delay(1000);
  }
  for (byte i = 8; i < 120; i++) {
    Wire.beginTransmission(i);
    if (Wire.endTransmission() == 0) {
      switch (i) {
        case I2C_DISPLAY_ADDRESS:
          Serial.println("Trouvé afficheur OLED");
          drawBoot(&display, "I2C", "Trouvé afficheur OLED", counter);
          delay(1000);
          break;
        case I2C_ADS1115_ADDRESS:
          Serial.println("Trouvé proxy I2C ADS1115");
          if (oled_display) {
            ads1115_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé CAN ADS1115", counter);
            delay(1000);
          }
          break;
        case I2C_SHTC3_ADDRESS:
          Serial.println("Trouvé SHTC3");
          if (oled_display) {
            shtc3_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé SHTC3", counter);
            delay(1000);
          }
          break;
        case I2C_AHT10_ADDRESS:
          Serial.println("Trouvé AHT10");
          if (oled_display) {
            aht10_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé AHT10", counter);
            delay(1000);
          }
          break;
        case I2C_BME280_ADDRESS:
          Serial.println("Trouvé BME280");
          if (oled_display) {
            BME280_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé BME280", counter);
            delay(1000);
          }
          break;
        case I2C_BMP180_ADDRESS:
          Serial.println("Trouvé BMP180");
          if (oled_display) {
            BMP180_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé BMP180", counter);
            delay(1000);
          }
          break;
        case I2C_BH1750FVI_ADDRESS:
          Serial.println("Trouvé BH1750FVI");
          if (oled_display) {
            bh1750fvi_i2c = true;
            number_of_sensor++;
            drawBoot(&display, "I2C", "Trouvé BH1750FVI", counter);
            delay(1000);
          }
          break;
        default:
          Serial.println("Trouvé appareil I2C (0x" + String(i, HEX) + ")");
          if (oled_display) {
            drawBoot(&display, "I2C", "Appareil non identifié  (0x" + String(i, HEX) + ")", counter);
            delay(1000);
          }
          break;
      }
      count++;
    }
  }
  Serial.println("Scan I2C fini " + String(count, HEX) + " Apareil(s) I2C.");
  if (oled_display) { 
    drawBoot(&display, "I2C", "Trouvé(s) " + String(count) + " Apareil(s)", 2);
    delay(1000);
  }
}

void wifiConnect() {
  int counter = 0;
  Serial.println("Commence à se connecter au Wifi");
  WiFi.begin(WIFI_SSID, WIFI_PWD);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    if (oled_display) {
      drawBoot(&display, "WiFi", "Se connecte", counter);
      delay(500);
    }
    counter++;
  }
  Serial.println("\nWifi connecté");
  if (oled_display) { 
    drawBoot(&display, "WiFi", "Connecté", 2);
    delay(1000);
  }  
}

void startmDNS() {
  Serial.println("Initialise mDNS");
  int counter = 0;
  counter = 0;
  if (oled_display) { 
    drawBoot(&display, "mDNS", "Initialisation", counter);
    delay(500);
  }
  counter++;
  if (oled_display) { 
    drawBoot(&display, "mDNS", "Initialisation", counter);
    delay(500);
  }
  counter++;
  // Démarre mDNS
  if (MDNS.begin("esp8266")){
    Serial.println("mDNS a démarré");
    if (oled_display) {
      drawBoot(&display, "mDNS", "A démarré", 2);
      delay(1000);
    }
  } else {
    Serial.println("mDNS non démarré");
    if (oled_display) {
      drawBoot(&display, "mDNS", "non démarré", 2);
      delay(1000);   
    }
  }
}

String getHeure(int digits) {
  struct tm* heureInfo;

  localnow = time(nullptr);
  heureInfo = localtime(&localnow);
  if(digits == 1) {
    char heure[12];
    sprintf_P(heure, PSTR("%02d"), heureInfo->tm_hour);
    return String(heure);  
  } else if(digits == 2) {
    char heure[14];
    sprintf_P(heure, PSTR("%02d:%02d"), heureInfo->tm_hour, heureInfo->tm_min);
    return String(heure);
  } else {
    char heure[16];
    sprintf_P(heure, PSTR("%02d:%02d:%02d"), heureInfo->tm_hour, heureInfo->tm_min, heureInfo->tm_sec);
    return String(heure);
  }
}

String getHeureSimple() {
  struct tm* heureInfo;
  localnow = time(nullptr);
  heureInfo = localtime(&localnow);
}

String getDate() {
  struct tm* timeInfo;
  char buff[16];
  now = time(nullptr);
  timeInfo = localtime(&now);
  //String date = WDAY_NAMES[timeInfo->tm_wday];
  sprintf_P(buff, PSTR("%s %02d %s %04d"), WDAY_NAMES[timeInfo->tm_wday].c_str(), timeInfo->tm_mday, MONTH_NAMES[timeInfo->tm_mon].c_str(), timeInfo->tm_year + 1900);
  return String(buff);
}

void setReadyForWeatherUpdate() {
  Serial.println("Configuration readyForUpdate à vrai");
  readyForWeatherUpdate = true;
}
