#define logo_weather_width 128
#define logo_weather_height 45
const unsigned char logo_weather[] PROGMEM = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 
    0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x01, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x00, 0x7C, 0x00, 
    0x00, 0xC0, 0x01, 0x00, 0x00, 0x00, 0x80, 0x60, 0x10, 0x00, 0x00, 0x00, 
    0xF8, 0x01, 0x7F, 0x00, 0x00, 0xC0, 0x01, 0x00, 0x00, 0x00, 0x80, 0x60, 
    0x10, 0x00, 0x00, 0x00, 0xC0, 0xC1, 0x7F, 0x00, 0x60, 0xC0, 0x01, 0x03, 
    0x80, 0x00, 0x00, 0x66, 0x06, 0x00, 0x00, 0x00, 0xC0, 0xE1, 0x61, 0x00, 
    0xE0, 0x00, 0x80, 0x03, 0xC0, 0x00, 0x00, 0x6E, 0x07, 0xF0, 0xFF, 0xFF, 
    0xFF, 0xE1, 0xE0, 0x00, 0xE0, 0x01, 0xC0, 0x03, 0xC0, 0x01, 0x00, 0xFC, 
    0x03, 0xF8, 0xFF, 0xFF, 0xFF, 0x70, 0xC0, 0x00, 0xC0, 0x01, 0xC0, 0x01, 
    0xE0, 0x01, 0x00, 0xF8, 0x01, 0xF0, 0xFF, 0xFF, 0x3F, 0x70, 0xC0, 0x01, 
    0x00, 0xF0, 0x07, 0x00, 0xC0, 0xC0, 0x90, 0xFF, 0x9F, 0x00, 0x00, 0x00, 
    0x00, 0x38, 0x80, 0x07, 0x00, 0xF8, 0x0F, 0x00, 0x00, 0xE0, 0xB0, 0xFF, 
    0xDF, 0xF0, 0xFF, 0x7F, 0x00, 0x38, 0x00, 0x7F, 0x00, 0x3C, 0x1E, 0x00, 
    0x00, 0xF0, 0x01, 0xF8, 0x01, 0xF8, 0xFF, 0xFF, 0x01, 0x38, 0x00, 0x7C, 
    0x00, 0x0E, 0x38, 0x00, 0x00, 0xF0, 0x01, 0xFC, 0x03, 0xF0, 0xFF, 0xFF, 
    0x03, 0x38, 0x00, 0x70, 0x00, 0x06, 0x30, 0x00, 0x00, 0xF0, 0x01, 0x6E, 
    0x07, 0x00, 0x00, 0x00, 0x03, 0x38, 0x00, 0x70, 0x00, 0x07, 0x70, 0x00, 
    0x80, 0xF0, 0x01, 0x66, 0x06, 0x00, 0x00, 0x00, 0x03, 0x38, 0x00, 0x70, 
    0x3E, 0x07, 0x70, 0x3E, 0xC0, 0xC1, 0x00, 0x62, 0x04, 0x00, 0x00, 0xF0, 
    0x03, 0x70, 0x00, 0x30, 0x3E, 0x07, 0x70, 0x3E, 0xE0, 0x03, 0x80, 0x60, 
    0x10, 0x00, 0x00, 0xF0, 0x01, 0x70, 0x00, 0x38, 0x00, 0x07, 0x70, 0x00, 
    0xF0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0xE0, 0x00, 0x1C, 
    0x00, 0x06, 0x30, 0x00, 0xF0, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0xC0, 0xE0, 0x01, 0x1E, 0x00, 0x0E, 0x38, 0x00, 0xF8, 0x0F, 0x00, 0x60, 
    0x00, 0x00, 0x80, 0x0F, 0xE0, 0xC0, 0xFF, 0x0F, 0x00, 0x3C, 0x1E, 0x00, 
    0xF8, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x80, 0x07, 0xF0, 0x01, 0xFF, 0x03, 
    0x00, 0xF8, 0x0F, 0x00, 0xF8, 0x0F, 0xF8, 0x01, 0x00, 0x00, 0xC0, 0x07, 
    0xF8, 0x07, 0xFC, 0x00, 0x00, 0xF0, 0x07, 0x00, 0xF8, 0x07, 0xFE, 0x07, 
    0x00, 0xE0, 0xC7, 0x03, 0xFE, 0x0F, 0x00, 0x00, 0xC0, 0x01, 0xC0, 0x01, 
    0xF0, 0x07, 0xFF, 0x0F, 0x00, 0xE0, 0xC3, 0x03, 0xF8, 0x07, 0x00, 0x00, 
    0xE0, 0x01, 0xC0, 0x03, 0xE0, 0x83, 0x07, 0x1E, 0x00, 0xF0, 0xE3, 0x01, 
    0xF0, 0x01, 0x00, 0x00, 0xE0, 0x00, 0x80, 0x03, 0x00, 0xC0, 0x01, 0x38, 
    0x00, 0xF0, 0xE1, 0x0F, 0xE0, 0x00, 0x00, 0x00, 0x60, 0xC0, 0x01, 0x03, 
    0x00, 0xC0, 0x01, 0x38, 0x00, 0xF8, 0xE1, 0x07, 0xC0, 0x00, 0x00, 0x00, 
    0x00, 0xC0, 0x01, 0x00, 0x00, 0xE0, 0x00, 0x70, 0x00, 0xF8, 0x80, 0x03, 
    0x40, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x01, 0x00, 0x00, 0xF0, 0x00, 0xF0, 
    0x03, 0xF8, 0x80, 0x03, 0x00, 0x00, 0x10, 0x00, 0x00, 0xC0, 0x01, 0x00, 
    0x00, 0xFC, 0x00, 0xF0, 0x0F, 0x7C, 0x80, 0x01, 0x00, 0x00, 0x30, 0x00, 
    0x00, 0x80, 0x00, 0x00, 0x00, 0x1E, 0x00, 0x00, 0x1E, 0xFC, 0xC7, 0x00, 
    0x00, 0x00, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 
    0x1C, 0xFC, 0xC3, 0x80, 0x00, 0x00, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x07, 0x00, 0x00, 0x38, 0xFE, 0x41, 0x80, 0x01, 0x00, 0xFE, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x38, 0xE0, 0x21, 0xE0, 
    0x03, 0x00, 0x38, 0x00, 0x00, 0xFE, 0xFF, 0x1F, 0x00, 0x07, 0x00, 0x00, 
    0x38, 0xE0, 0x20, 0xC0, 0x01, 0x00, 0x30, 0x00, 0x00, 0xFE, 0xFF, 0x1F, 
    0x00, 0x07, 0x00, 0x00, 0x38, 0x70, 0x00, 0x80, 0x00, 0x00, 0x10, 0x00, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x38, 0x70, 0x00, 0x00, 
    0x00, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 
    0x18, 0x38, 0x00, 0x00, 0x00, 0x0E, 0x00, 0x00, 0xC0, 0xFF, 0xFF, 0xF9, 
    0x07, 0x0E, 0x00, 0x00, 0x1C, 0x18, 0x00, 0x00, 0x80, 0x1F, 0x00, 0x00, 
    0xE0, 0xFF, 0xFF, 0xF9, 0x07, 0xFC, 0xFF, 0xFF, 0x0F, 0x18, 0x00, 0x00, 
    0xC0, 0x3F, 0x00, 0x00, 0x80, 0xE4, 0xE3, 0x00, 0x00, 0xF8, 0xFF, 0xFF, 
    0x07, 0x0C, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0xE0, 0xFF, 0xFF, 0x01, 0x04, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 
    0x00, 0xF0, 0xFF, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 
    0x00, 0x06, 0x00, 0x00, 0x00, 0xF8, 0xFF, 0x7F, 0x00, 0x00, 0x00, 0x00, 
    0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF0, 0xFF, 0x3F, 
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  
};
