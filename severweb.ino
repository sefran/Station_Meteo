void initWEBConfig() {
  server.on("/", handleRoot);
  server.on("/local.css", handleLocalCSS);
  if(DHT_sensor) {
    server.on("/DHT", handleDHTSensor);
    server.on("/DHT/temperature", handleDHTTemperature);
    server.on("/DHT/humidity", handleDHTHumidity);
  } else {
    server.on("/DHT", handleDHTSensor);
  };
  if(SHTC3_sensor) {
    server.on("/SHTC3", handleSHTC3Sensor);
    server.on("/SHTC3/temperature", handleSHTC3Temperature);
    server.on("/SHTC3/humidity", handleSHTC3Humidity);
  } else {
    server.on("/SHTC3", handleSHTC3Sensor);
  };
  if(AHT10_sensor) {
    server.on("/AHT10", handleAHTSensor);
    server.on("/AHT10/temperature", handleAHTTemperature);
    server.on("/AHT10/humidity", handleAHTHumidity);
  } else {
    server.on("/AHT10", handleAHTSensor);
  };
  if(BME280_sensor) {
    server.on("/BME280", handleBME280Sensor);
    server.on("/BME280/temperature", handleBME280Temperature);
    server.on("/BME280/humidity", handleBME280Humidity);
    server.on("/BME280/pressure", handleBME280Pressure);
  } else {
    server.on("/BME280", handleBME280Sensor);
  };
  if(BMP180_sensor) {
    server.on("/BMP180", handleBMP180Sensor);
    server.on("/BMP180/temperature", handleBMP180Temperature);
    server.on("/BMP180/pressure", handleBMP180Pressure);
  } else {
    server.on("/BMP180", handleBMP180Sensor);
  };
  if(BH1750FVI_sensor) {
    server.on("/BH1750", handleBH1750Sensor);
    server.on("/BH1750/light", handleBH1750Lighting);
  } else {
    server.on("/BH1750", handleBH1750Sensor);
  };
  if(RAIN_sensor) {
    server.on("/rain", handleRainSensor);
    server.on("/rain/value", handleRainValue);
    server.on("/rain/text", handleRainText);
  } else {
    server.on("/rain/sensor", handleRainSensor);
  };
  if(RAINLEVEL_sensor) {
    server.on("/rainlevel", handleRainLevelSensor);
    server.on("/rainlevel/value", handleRainLevelValue);
    server.on("/rainlevel/week", handleRainLevelWeekValue);
    server.on("/rainlevel/month", handleRainLevelMonthValue);
    server.on("/rainlevel/year", handleRainLevelYearValue);
  } else {
    server.on("/rainlevel", handleRainLevelSensor);
  };
  if(WEATHERCOCK_sensor) {
    server.on("/weathercock", handleWeathercockSensor);
    server.on("/weathercock/rotate", handleWeathercockRotate);
    server.on("/weathercock/direction", handleWeathercockDirection);
  } else {
    server.on("/weathercock", handleWeathercockSensor);
  };
  if(ANEMOMETER_sensor) {
    server.on("/anemometer", handleAnemometerSensor);
    server.on("/anemometer/speed", handleAnemometerSpeed);
    server.on("/anemometer/gust", handleAnemometerGust);
  } else {
    server.on("/anemometer", handleAnemometerSensor);
  };
  server.onNotFound(handleNotFound);
  server.begin();
}

void handleNotFound() {
  // Page 404 du site
  String message = "Fichier non trouvé\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  digitalWrite(LED_PIN, 1);
  server.send(404, "text/plain;charset=utf-8", message);
  digitalWrite(LED_PIN, 0);
}

void handleRoot() {
  // Configuration de la page WEB Racine
  char htlm_header[350];
  char htlm_day_body[150];
  char htlm_star_weather_info_body[50];
  char htlm_temperature_body[100];
  char htlm_start_descrition_body[50];
  char htlm_humidity_body[80];
  char htlm_place_body[100];
  char htlm_end_descrition_body[30];
  char htlm_end_weather_info_body[30];
  char htlm_pressure_body[55];
  char htlm_footer_body[100];
  char page_html[1100];
  char localsky[30];

  // Calcul de l'heure de l'ESP8266
  int sec = millis()/1000;
  int minut = sec/60;
  int hr = minut/60;
  minut = minut%60;
  sec = sec%60;

  // Configuration du site WEB
  if(light_sensor) {
    if(rain_sensor) {
      if (locallux > DAY_LIGHT_VALUE){
        if (localrain < 50) {
          snprintf(localsky, 30, "wi-day-sunny");
        } else if (localrain < 300) {
          snprintf(localsky, 30, "wi-day-rain-mix");
        } else if (localrain > 400) {
          snprintf(localsky, 30, "wi-day-rain");  
        } else {
          snprintf(localsky, 30, "wi-day-sprinkle");
        }
      } else {
        if (localrain < 50) {
          snprintf(localsky, 30, "wi-stars");
        } else if (localrain < 300) {
          snprintf(localsky, 30, "wi-night-alt-rain-mix");
        } else if (localrain > 400) {
          snprintf(localsky, 30, "wi-night-alt-rain");  
        } else {
          snprintf(localsky, 30, "wi-night-alt-sprinkle");
        }    
      } 
    } else if (locallux > DAY_LIGHT_VALUE){
        snprintf(localsky, 30, "wi-day-sunny");
      } else {
        snprintf(localsky, 30, "wi-stars");
      }
  } else {
    snprintf(localsky, 30, "wi-alien");
  }

  // Écriture du code HTML de la page
  // Génération de l'entête HTML
  // <meta http-equiv="refresh" content="3;url=http://www.monsite.fr/" />
  snprintf(htlm_header, 350, "\
<!DOCTYPE html>\
<html lang=\"fr\">\
  <head>\
    <meta charset=\"UTF-8\"/>\
    <meta http-equiv=\"refresh\" content=\"5;URL='/'\"/>\
    <title>%s</title>\
    <link rel=\"stylesheet\" type=\"text/css\" href=\"local.css\" />\
  </head>\
           ", LOCAL_CITY
  );
          
  // Génération du pictogramme météo
  snprintf(htlm_day_body, 150, "\
  <body>\
    <article class=\"widget\">\
    <div class=\"weatherIcon\">\
        <i class=\"wi %s\"></i>\
    </div>\
           ", localsky
  );
          
  // Génération de la div d'information météorologique
  snprintf(htlm_star_weather_info_body, 50, "\
    <div class=\"weatherInfo\">\
           "
  );
          
  if(temperature_sensor) {
    // Génération div température
    snprintf(htlm_temperature_body, 100, "\
        <div class=\"temperature\"><span>%.1f°</span></div>\
           ", localtemp
    );
  };
          
  // Génération div description
  snprintf(htlm_start_descrition_body, 50, "\
        <div class=\"description\">\
           "
  );
          
  if(humidity_sensor) {
    // Génération div humidité
    snprintf(htlm_humidity_body, 80, "\
            <div class=\"weatherCondition\">Humidité %.0f%%</div>\
           ", localhumidity
    );
  };
          
  // Génération div localité
  snprintf(htlm_place_body, 100, "\
            <div class=\"place\">%s, %s</div>\
           ", LOCAL_CITY, LOCAL_STATE
  );
          
  // Génération fin div description
  snprintf(htlm_end_descrition_body, 30, "\
        </div>\
           "
  );
          
  // Fin de la div d'information météorologique
  snprintf(htlm_end_weather_info_body, 30, "\
    </div>\
           "
  );
          
  if(pressure_sensor) {
    // Génération div pression
    snprintf(htlm_pressure_body, 55, "\
    <div class=\"date\">%.0f hPa</div>\
           ", localpressure
    );
  };
          
  // Génération du pied du corps HTML
  snprintf(htlm_footer_body, 100, "\
    </article>\
    <p>Temps de fonctionnement: %02d:%02d:%02d</p>\
  </body>\
</html>\
           ", hr, minut, sec
  );

  snprintf(page_html, 1100, "%s%s%s%s%s%s%s%s%s%s%s", htlm_header, htlm_day_body, htlm_star_weather_info_body, htlm_temperature_body, htlm_start_descrition_body, htlm_humidity_body, htlm_place_body, htlm_end_descrition_body, htlm_end_weather_info_body, htlm_pressure_body, htlm_footer_body);
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/html", page_html);
  digitalWrite(LED_PIN, 0);
}

void handleDHTSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(DHT_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleDHTTemperature() {
  digitalWrite(LED_PIN, 1);
  float tempdht = dht.readTemperature();
  server.send(200, "text/plain;charset=utf-8", String(getDHTxxTemperature(), 1));
  digitalWrite(LED_PIN, 0);
}

void handleDHTHumidity() {
  digitalWrite(LED_PIN, 1);
  float humdht = dht.readHumidity();
  server.send(200, "text/plain;charset=utf-8", String(getDHTxxHumidity(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleSHTC3Sensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(SHTC3_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleSHTC3Temperature() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getSHTC3Temperature(), 1));
  digitalWrite(LED_PIN, 0);
}

void handleSHTC3Humidity() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getSHTC3Humidity(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleAHTSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(AHT10_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleAHTTemperature() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getAHT10Temperature(), 1));
  digitalWrite(LED_PIN, 0);
}

void handleAHTHumidity() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getAHT10Humidity(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleBME280Sensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(BME280_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleBME280Temperature() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBME280Temperature(), 1));
  digitalWrite(LED_PIN, 0);
}

void handleBME280Humidity() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBME280Humidity(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleBME280Pressure() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBME280Pressure(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleBME280Altitude() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBME280Altitude(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleBMP180Sensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(BMP180_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleBMP180Temperature() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBMP180Temperature(), 1));
  digitalWrite(LED_PIN, 0);
}

void handleBMP180Pressure() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBMP180Pressure(), 0));
  digitalWrite(LED_PIN, 0);
}

void handleBH1750Sensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(BH1750FVI_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleBH1750Lighting() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getBH1750Light()));
  digitalWrite(LED_PIN, 0);
}

void handleRainSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(RAIN_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleRainValue() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getSensorRain()));
  digitalWrite(LED_PIN, 0);
}

void handleRainText() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", getLocalRainText());
  digitalWrite(LED_PIN, 0);
}

void handleRainLevelSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(RAINLEVEL_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleRainLevelValue() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getLocalRainLevel()));
  digitalWrite(LED_PIN, 0);
}

void handleRainLevelWeekValue() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String("Non implémenté"));
  digitalWrite(LED_PIN, 0);
}

void handleRainLevelMonthValue() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String("Non implémenté"));
  digitalWrite(LED_PIN, 0);
}

void handleRainLevelYearValue() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String("Non implémenté"));
  digitalWrite(LED_PIN, 0);
}

void handleWeathercockSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(WEATHERCOCK_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleWeathercockRotate() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getSensorWeathercock()));
  digitalWrite(LED_PIN, 0);
}

void handleWeathercockDirection() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getLocalWeathercock()));
  digitalWrite(LED_PIN, 0);
}

void handleAnemometerSensor() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(ANEMOMETER_sensor));
  digitalWrite(LED_PIN, 0);
}

void handleAnemometerGust() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getLocalAnemometerRotate()));
  digitalWrite(LED_PIN, 0);
}

void handleAnemometerSpeed() {
  digitalWrite(LED_PIN, 1);
  server.send(200, "text/plain;charset=utf-8", String(getLocalAnemometerGust()));
  digitalWrite(LED_PIN, 0);
}
