void drawLocalWeather(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  if (show_temperature) {
    display->setFont(ArialMT_Plain_24);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    display->drawString(72 + x, 11 + y, getDisplayTemperature());
  }

  if (show_humidity) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getDisplayHumidity() != "None"){
      display->drawString(85 + x, 38 + y, getDisplayHumidity());
    }
  }

  if (show_pressure) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getDisplayPressure() != "None"){
      display->drawString(105 + x, 0 + y, getDisplayPressure());
    }  
  }

  if(show_light) {
    // Lecture de la luminosite
    //locallux;
    //if (getDisplayLux() != "None"){
    //  display->drawString(22 + x, 20 + y, getDisplayLux());
    //}
  }

  if (show_rain) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getLocalRainText() != "None"){
      display->drawString(22 + x, 10 + y, getLocalRainText());
    }
  }

  if(show_rainlevel) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getDisplayRainlevel() != "None"){
      display->drawString(22 + x, 38 + y, getDisplayRainlevel());
    }
  }

  if (show_weathercock) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getDisplayWeathercock() != "None"){
      display->drawString(55 + x, 0 + y, getDisplayWeathercock());
    }
  }

  if(show_anemometer) {
    display->setFont(ArialMT_Plain_10);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    if (getDisplayAnemometerGust() != "None"){
      display->drawString(18 + x, 0 + y, getDisplayAnemometerGust());
    }
  }
}
