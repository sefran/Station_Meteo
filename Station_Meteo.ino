#include <Arduino.h>

// Timer
#include "Ticker.h"

// Réseau et serveur WEB
#include "ESP8266WiFi.h"
#include "ESP8266HTTPClient.h"
#include "ESP8266WebServer.h"
#include "ESP8266mDNS.h"
#include "JsonListener.h"
#include "ArduinoJson.h"

// I2C
#include <Wire.h>

// Date Heure
#include "time.h" // time() ctime()
#include "sys/time.h" // struct timeval
#include "coredecls.h" // settimeofday_cb()

// Affichage
#include <SSD1306Wire.h>
#include <OLEDDisplayUi.h>

// Météo internet
#include "OpenWeatherMapCurrent.h"
#include "OpenWeatherMapForecast.h"
#include "weatherStationFonts.h"
#include "weatherStationImages.h"

// Capteurs
#include <SPI.h>
#include <DHT.h>
//#include <SparkFun_SHTC3.h>
#include <ClosedCube_SHTC3.h>
#include <Adafruit_AHT10.h>
#include <SparkFunBME280.h>
#include <Adafruit_BMP085.h>
#include <BH1750FVI.h>
#include <ADS1X15.h>

// Configurations
#include "Configs.h" // Congiguration manuelle de la station météo
#include "ConfigsPwd.h" // Configuration manuelle confidentielle des mots de passe et identifiants
#include "ConfigsProg.h" // Ne modifier que pour améliorer ce programme

/**********
 * Setup  *
 **********/

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println("Démarre l'initialisation de la station météo");
  Serial.println();

  // Configuration de la LED d'accès WEB
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, 0);

  // Démarre l'I2C
  initI2C();

  // active l'afficheur si présent
  if (oled_display) {
    initDisplay();
  }

  // Scan I2C (init_weather_station.ino) 
  scanI2C();
  
  // active le Wifi
  wifiConnect();

  // active l'accès DNS
  startmDNS();

  // Initialisation des capteurs de la station météo (init_weather_station.ino)
  //Serial.println("Initialise les capteurs de la sonde météo");
  initHardwareConfig();
    
  // Initialise l'affichage météo de l'afficheur OLED
  if (oled_display) {
    numberOfFrames = 2;
    initWeatherUI();
  }  

  // Initialisation de la mesure des capteurs
  initSensors();

  Serial.println("Fin de l'initialisation de la station Météo");
}

/**********************
 * Boucle principale  *
 **********************/

void loop() {
  if (getsensorsvalues) {
    if (ANEMOMETER_sensor) {
      if (getmesurementanemometer == ANEMOMETER_MEASUREMENT_INTERVAL_GUST * BASE_TIME_MESUREMENT_ANEMOMETER) {
        Serial.println("Mesure de la vitesse du vent");
        anemometerCalculateWindSpeed();
        getmesurementanemometer = 0;
      };
      getmesurementanemometer++;
    }

    if (getmesurementsensors == 0) {
      getValuesSensors();
      getmesurementsensors = MESUREMENT_SENSORS_INTERVAL * BASE_TIME_MESUREMENT_SENSOR;
    }
    getmesurementsensors--;
    getsensorsvalues = false;
  }
  
  //if (millis() - timeSinceLastWUpdate > (UPDATE_INTERVAL_SECS * 1000)) {
  //  Serial.println("Relève la météo internet");    
  //  //setReadyForWeatherUpdate();
  //  timeSinceLastWUpdate = millis();
  //}

  if (oled_display) {
    remainingTimeBudget = ui.update();
        
    if (remainingTimeBudget > 0) {
      delay(remainingTimeBudget);
    }

    //if (readyForWeatherUpdate && ui.getUiState()->frameState == FIXED) {
    //  remainingTimeBudget = ui.update();
    //}
  }
  server.handleClient();
  MDNS.update();
}
