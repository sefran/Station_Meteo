void initI2C() {
  Serial.println("Initialise l'I2C");
  #if USER_PIN
    Wire.begin(SDA_PIN, SDC_PIN);
  #else
    Wire.begin();
  #endif

  // Détecte si l'afficheur est présent
  for (byte i = 8; i < 120; i++) {
    if (i == I2C_DISPLAY_ADDRESS) {
      Wire.beginTransmission(i);
      if (Wire.endTransmission() == 0) {
        Serial.println("Appareil I2C présent comme Afficheur");
        oled_display = true;
      }
    }
  }  
}

void initDisplay() {
    Serial.println("Initialise l'afficheur");
    display.init();
    display.clear();
    display.display();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setContrast(255);
}

void initWeatherUI() {
  // Activation de l'affichage Météo sur l'afficheur
  Serial.println("Activation de l'affichage Météo sur l'afficheur");
  // Configuration de l'afficheur
  ui.init();
  ui.setTargetFPS(DISPLAY_FPS);
  ui.setActiveSymbol(activeSymbole);
  ui.setInactiveSymbol(inactiveSymbole);
  ui.setIndicatorPosition(DISPLAY_POSITION);
  ui.setIndicatorDirection(LEFT_RIGHT);
  ui.setFrameAnimation(DISPLAY_ANIMATION);
  if(numberOfFrames == 1){
    ui.setFrames(framestime, numberOfFrames);
  } else if(numberOfFrames == 2) {
    ui.setFrames(frameslocal, numberOfFrames);
  } else if(numberOfFrames == 3) {
    ui.setFrames(framesnet, numberOfFrames);
  } else if(numberOfFrames == 4) {
    ui.setFrames(framesall, numberOfFrames);
  }
  ui.setOverlays(overlays, numberOfOverlays);
}

bool initDHT() {
  pinMode(DHT_PIN, INPUT);
  dht.begin();
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(t) || isnan(h)) {
    return false;
  } else {
    return true;
  }
}

void initHardwareConfig() {
  Serial.println("Initialise les capteurs");

  // Ajout des capteurs déclarés manuellement
  if (dht_connect.equals("esp8266") or dht_connect.equals("ads1115")) { number_of_sensor++; };
  if (humidity_ground_connect.equals("esp8266") or humidity_ground_connect.equals("ads1115")) { number_of_sensor++; };
  if (rain_connect.equals("esp8266") or rain_connect.equals("ads1115")) { number_of_sensor++; };
  if (rain_drops_connect.equals("esp8266") or rain_drops_connect.equals("ads1115")) { number_of_sensor++; };
  if (rainlevel_connect) { number_of_sensor++; };
  if (weathercock_connect.equals("esp8266") or weathercock_connect.equals("ads1115")) { number_of_sensor++; };
  if (anemometer_connect) { number_of_sensor++; };

  byte progress = 0;
  byte step = 100 / ((number_of_sensor + 1) * 2 + 4);

  // active le Convertisseur Analogique Numéraque 16bits ADS1115
  if (ads1115_i2c) {
    Serial.println("Configuration CAN ADS1115");
    if (oled_display) {
      drawProgress(&display, progress, "I2C CAN ADS1115", "Configuration ");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du CAN I2C ADS1115
    adscan.begin();
    if (oled_display) {
      drawProgress(&display, progress, "I2C CAN ADS1115", "OK");
      delay(1000);
    }
    progress = progress + step;  
  }

  // active le capteur DHT11
  if (dht_connect != "0") {
    Serial.println("Configuration DHT11");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur DHT11", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du capteur DHT11
    if (initDHT()){
      DHT_sensor = true;
      temperature_sensor = true;
      humidity_sensor = true;
      if (oled_display) {
        show_temperature = true;
        show_humidity = true;
        drawProgress(&display, progress, "Capteur DHT11", "OK");
      }
    } else {
      Serial.println("DHT11 introuvable! Vérifier le branchement");
      if (oled_display) {
        drawProgress(&display, progress, "Capteur DHT11", "Vérifier le branchement");
      }  
    }
    
    if (oled_display) {
      delay(1000);
    }
    progress = progress + step;  
  }

  // active le capteur SHTC3
  if (shtc3_i2c) {
    Serial.println("Configuration SHTC3");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur SHTC3", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du capteur SHTC3
    shtc3.address(I2C_SHTC3_ADDRESS); // I2C Address: 0x70
    shtc3.wakeUp();
    //if (shtc3.begin()){
    if (shtc3.readId()){
      SHTC3_sensor = true;
      temperature_sensor = true;
      humidity_sensor = true;
      if (oled_display) {
        show_temperature = true;
        show_humidity = true;
        shtc3.sleep();
        drawProgress(&display, progress, "Capteur SHTC3", "OK");
      }
    } else {
      Serial.println("SHTC3 introuvable! Vérifier le branchement");
      if (oled_display) {
        drawProgress(&display, progress, "Capteur SHTC3", "Vérifier le branchement");
      }
    }
    
    if (oled_display) {
      delay(1000);
    }
    progress = progress + step;
  }  
  
  // active le capteur AHT10
  if (aht10_i2c) {
    Serial.println("Configuration AHT10");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur AHT10", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du capteur AHT10
    if (aht.begin()){
      AHT10_sensor = true;
      aht_temp = aht.getTemperatureSensor();
      aht_temp->printSensorDetails();
      temperature_sensor = true;
      if (oled_display) {
        show_temperature = true;
        drawProgress(&display, progress, "Capteur AHT10", "Température OK");
        delay(1000);
      }
      aht_humidity = aht.getHumiditySensor();
      aht_humidity->printSensorDetails();
      humidity_sensor = true;
      if (oled_display) {
        show_humidity = true;
        drawProgress(&display, progress, "Capteur AHT10", "Humidité OK");
        delay(1000);
      }
      if (oled_display) {
        drawProgress(&display, progress, "Capteur AHT10", "OK");
      }
    } else {
      Serial.println("AHT10 introuvable! Vérifier le branchement");
        if (oled_display) {
          drawProgress(&display, progress, "Capteur AHT10", "Vérifier le branchement");
        }
    }
    
    if (oled_display) {
      delay(1000);
    }
    progress = progress + step;
  }  
  
  // active le capteur BME280
  if (BME280_i2c) {
    Serial.println("Configuration BME280");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur BME280", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du capteur BME280
    bme280.setI2CAddress(I2C_BME280_ADDRESS);
    if (bme280.beginI2C()) {
      BME280_sensor = true;
      temperature_sensor = true;
      humidity_sensor = true;
      pressure_sensor = true;
      altitude_sensor = true;
      if (oled_display) {
        show_temperature = true;
        show_humidity = true;
        show_pressure = true;
        show_altitude = true;
        drawProgress(&display, progress, "Capteur BME280", "OK");
      }
    } else {
      if (oled_display) {
        Serial.println("BME280 introuvable! Vérifier le branchement");
        drawProgress(&display, progress, "Capteur BME280", "Vérifier le branchement");
      }
    }
    delay(1000);
    progress = progress + step;
  }  
 
  // active le capteur BMP180
  if (BMP180_i2c) {
    Serial.println("Configuration BMP180");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur BMP180", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    // initialisation du capteur BMP180
    if (bmp180.begin()) {
      BMP180_sensor = true;
      temperature_sensor = true;
      pressure_sensor = true;
      altitude_sensor = true;
      if (oled_display) {
        show_temperature = true;
        show_pressure = true;
        show_altitude = true;
        drawProgress(&display, progress, "Capteur BMP180", "OK");
      }
    } else {
      if (oled_display) {
        Serial.println("BMP180 introuvable! Vérifier le branchement");
        drawProgress(&display, progress, "Capteur BMP180", "Vérifier le branchement");
      }
    }
    delay(1000);
    progress = progress + step;
  }  
 
  // active le capteur BH1750FVI
  if (bh1750fvi_i2c) {
    Serial.println("Configuration BH1750FVI");
    if (oled_display) {
      drawProgress(&display, progress, "Capteur BH1750FVI", "Configuration");
      delay(1000);
    }
    progress = progress + step;
  
    // active le capteur BH1750FVI
    lightSensor.begin();
    if (lightSensor.isConnected()) {
      lightSensor.powerOn();
      lightSensor.setContHighRes();
      BH1750FVI_sensor = true;
      light_sensor = true;
      if (oled_display) {
        show_light = true;
        drawProgress(&display, progress, "Capteur BH1750FVI", "OK");
      }
    } else {
      Serial.println("BH1750 introuvable! Vérifier le branchement");
      if (oled_display) {
        drawProgress(&display, progress, "Capteur BH1750FVI", "Vérifier le branchement");
      }
    }
    
    if (oled_display) {
      delay(1000);
    }
    progress = progress + step;
  }  

  // active le capteur de détection de temps de pluie et de neige
  if (rain_connect != "0") {
    Serial.println("Variable Xinda : " + String(progress));
    if (oled_display) {
      drawProgress(&display, progress, "Niveau de pluie Xinda", "Configuration");
      delay(1000);
    }
    progress = progress + step;

    rain_sensor = true;
    if (oled_display) {
      show_rain = true;
      drawProgress(&display, progress, "Niveau de pluie Xinda", "OK");
      delay(1000);
    }
    progress = progress + step;
  }  

  // active le pluvionètre LEXCA009
  if (rainlevel_connect != "0") {
    Serial.println("Configuration LEXCA009");
    if (oled_display) {
      drawProgress(&display, progress, "Pluviomètre", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    attachInterrupt(digitalPinToInterrupt(LEXCA009_PIN), handleCountRainLevelPulses, RISING);
    rainlevel_sensor = true;
    if (oled_display) {
      show_rainlevel = true;
      drawProgress(&display, progress, "Pluviomètre", "OK");
      delay(1000);
    }
    progress = progress + step;
  }  
  
  // active le capteur de girouette LEXCA003
  if (weathercock_connect != "0") {
    Serial.println("Configuration LEXCA003");
    if (oled_display) {
      drawProgress(&display, progress, "Girouette", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    if (weathercock_connect != "0") {
      if (oled_display) {
        drawProgress(&display, progress, "Girouette", "Mauvais paramétrage");
        delay(1000);
      }      
    } else if(weathercock_connect.equals("esp8266")) {
      weathercock_sensor = true;
      if (oled_display) {
        show_weathercock = true;
        drawProgress(&display, progress, "Girouette", "Sur A0");
        delay(1000);
      }
    } else if(weathercock_connect.equals("ads1115")) {
      weathercock_sensor = true;
      if (oled_display) {
        show_weathercock = true;
        drawProgress(&display, progress, "Girouette", "ADS1115 sortie A" + String(weathercock_ads1115_adc));
        delay(1000);
      }
    } else {
      if (oled_display) {
        drawProgress(&display, progress, "Girouette", "Mauvaise conf weathercock_connect");
        delay(1000);
      }
    }
    progress = progress + step;
  }  
  
  // active le capteur de vitesse du vent LEXCA002
  if (anemometer_connect != "0") {
    Serial.println("Configuration LEXCA002");
    if (oled_display) {
      drawProgress(&display, progress, "Anémomètre", "Configuration");
      delay(1000);
    }
    progress = progress + step;
    
    localanemometerpulses = 0;
    localanemometerrpm = 0;
    localanemometerspeed = 0.0;
    anemometer_timeold = 0;
    gustadd = 0.0;
    gustnumber = 0;
    anemometer_timevalue = 0;
    pinMode(LEXCA002_PIN, INPUT_PULLUP);
    // branche une interruption de comptage de tours
    attachInterrupt(digitalPinToInterrupt(LEXCA002_PIN), handleCountPulses, RISING);
    // Initialize parameters
    anemometer_sensor = true;
    if (oled_display) {
      show_anemometer = true;
      drawProgress(&display, progress, "Anémomètre", "OK");
      delay(1000);
    }
    progress = progress + step;
  }  

  // Mise à jour Heure
  Serial.println("Variable Heure : " + String(progress));
  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpServer);
  if (oled_display) {
    drawProgress(&display, progress, "Heure", "Configuration");
    delay(1000);
  }
  // La chaîne de caractères "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00" est une "timezone string".
  // Avec cette configuration le changement d'heure sera donc automatique le dernier dimanche de mars à 2:00 et octobre à 3:00.
  progress = progress + step;

  Serial.println("Variable Météo Locale : " + String(progress));
  if (oled_display) {
    drawProgress(&display, progress, "Prév Météo Locale", "Configuration");
    delay(1000);
  }
  currentWeatherClient.setMetric(IS_METRIC);
  currentWeatherClient.setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  currentWeatherClient.updateCurrentById(&currentWeather, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID);
  progress = progress + step;

  Serial.println("Variable Météo 3J : " + String(progress));
  if (oled_display) {
    drawProgress(&display, progress, "Prév Météo Locale 3J", "Configuration");
    delay(1000);
  }
  forecastClient.setMetric(IS_METRIC);
  forecastClient.setLanguage(OPEN_WEATHER_MAP_LANGUAGE);
  uint8_t allowedHours[] = {12};
  forecastClient.setAllowedHours(allowedHours, sizeof(allowedHours));
  forecastClient.updateForecastsById(forecasts, OPEN_WEATHER_MAP_APP_ID, OPEN_WEATHER_MAP_LOCATION_ID, MAX_FORECASTS);
  progress = progress + step;

  Serial.println("Variable Serveur WEB : " + String(progress));
  if (oled_display) {
    drawProgress(&display, progress, "Serveur WEB", "Configuration");
    delay(1000);
  }
  
  //Serveur WEB
  initWEBConfig();
  Serial.println("Serveur HTTP démarré");

  Serial.println("Variable fin : " + String(progress));

  if (oled_display) {
    drawProgress(&display, 100, "Station Météo", "Démarrage terminé...");
    delay(1000);
  }

  readyForWeatherUpdate = false;
}

void initSensors() {
  timer1_isr_init();
  timer1_attachInterrupt(setSensorsMeasure); // Ratache un timer d'interruption à la fonction setSensorsMeasure()
  Serial.println("Mesure Capteurs");
  /* Diviseurs:
    TIM_DIV1 = 0,   //80MHz (de 12.5 nanosecondes à tous les 104.857588 milisecondes max)
    TIM_DIV16 = 1,  //5MHz (0.2 microsecondes à tous les 1.6777214 secondes max)
    TIM_DIV256 = 3  //312.5Khz (1 déclenchement tous les 3.2microsecondes à tous les 26.8435424 secondes max)
  Relance du timer:
    TIM_SINGLE  0 // La routine d'interruption, vous devez écrire une nouvelle valeur pour redémarrer le temporisateur
    TIM_LOOP  1 // Sur interruption le compteur recommencera avec la même valeur
  */
  timer1_enable(TIM_DIV256, TIM_EDGE, TIM_LOOP);
  timer1_write(312500); // Pour un TIM_DIV256 .2 microseconds × 312 500 soit 1s 
}
