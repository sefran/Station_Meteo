void drawHeaderOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  display->setColor(WHITE);
  display->setFont(ArialMT_Plain_10);
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->drawString(0, 54, getHeure(2));
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->drawString(128, 54, getDisplayTemperature());
  display->drawHorizontalLine(0, 52, 128);
}
