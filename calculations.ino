// Fonction de transfert de l'anémomètre à coupelles
#define TRANSFERT_FUNCTION 1.0
float transferFunction(int rpm) {
  Serial.println("Calcul de la fonction de transfer");
  return TRANSFERT_FUNCTION;
}

void anemometerCalculateWindSpeed() {
  int deltatime;
  //Don't process interrupts during calculations
  detachInterrupt(digitalPinToInterrupt(LEXCA002_PIN));
  deltatime = millis() - anemometer_timeold;
  localanemometerrpm = getAnemometerRotate(deltatime);
  localanemometergust =  getAnemometerGust(deltatime);
  gustadd += localanemometergust;
  gustnumber++;
  anemometer_timevalue += ANEMOMETER_MEASUREMENT_INTERVAL_GUST;
  if (anemometer_timevalue >= ANEMOMETER_MEASUREMENT_INTERVAL_SPEED) {
    //Cacul de la vitesse du vent
    localanemometerspeed = gustadd/gustnumber;
    gustadd = 0.0;
    gustnumber = 0;
    anemometer_timevalue = 0;
  }
  // Initialisation variables calculs
  anemometer_timeold = millis();
  localanemometerpulses = 0;
  Serial.print("RPM = ");
  Serial.println(localanemometerrpm,DEC);
  //Restart the interrupt processing
  attachInterrupt(digitalPinToInterrupt(LEXCA002_PIN), handleCountPulses, RISING);
}
