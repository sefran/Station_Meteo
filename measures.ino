float getLocalTemperature() {
  if(AHT10_sensor) {
    //return getAHT10Temperature();
    return 0.0;
  } else if(SHTC3_sensor) {
    return getSHTC3Temperature();
  } else if(BME280_sensor) {
    return getBME280Temperature();
  } else if(BMP180_sensor) {
    return getBMP180Temperature(); //plus précis que dht
  } else if(DHT_sensor) {
    return getDHTxxTemperature();
  }
}

float getLocalHumidity() {
  if(DHT_sensor) {
    return getDHTxxHumidity();
  } else if(AHT10_sensor) {
    return getAHT10Humidity();
  } else if(SHTC3_sensor) {
    return getSHTC3Humidity();
  }
}

String getLocalHumidityText() {
  float humidity = getLocalHumidity();
  if (humidity < 30.0){
    return "Trop sec";
  } else if (humidity < 40.0) {
    return "Sec";
  } else if (humidity > 65.0) {
    return "Trop humide";
  } else {
    return "Humidité normale";
  }
}

float getLocalPressure() {
  if(BME280_sensor) {
    return getBME280Pressure();
  } else if (BMP180_sensor) {
    return getBMP180Pressure();
  }
}

// altitude = 44330*(1-(P/p0)^(1/5.255)
float getLocalAltitude() {
  if(BME280_sensor) {
    return getBME280Altitude();
  } else if(BMP180_sensor) {
    return getBMP180Altitude();
  }
}

float getLocalLight() {
  if(BH1750FVI_sensor) {
    return getBH1750Light();
  }  
}

int getLocalRain() {
  if(RAIN_sensor) {
    return getSensorRain();
  }
}

bool getLocalRainDrops() {
  if(RAINDROPS_sensor) {
    return getSensorDrops();
  }
}

String getLocalRainText() {
  if (RAIN_sensor) {
    if (localrain < 50) {
      return "Sec";
    } else if (localrain < 300) {
      return "Bruine";
    } else if (localrain > 400) {
      return "Forte pluie";  
    } else {
      return "Pluie";
    }
  } else {
    return "None";
  }
}

float getLocalRainLevel() {
  if (RAINLEVEL_sensor) {
    return RAINLEVEL_VOLUME * getSensorRainLevel() / RAINLEVEL_PULSE_OFFSET;
  }
}

void rainGaugeSave() {
  if (getHeure(3) == "00:00:00") {
    if (flagheure) {
      flagheure = false;
      // TODO Sauvegarde de la pluiviométrie journalière
      rainlevel_pulses = 0;
    }
  }
  if (getHeure(3) == "12:00:00") {
    // Active la sauvegarde pour 00:00:00 du niveau de pluie
    flagheure = true;
  }
}

String getLocalWeathercock() {
  if (WEATHERCOCK_sensor) {
    if(localweathercock > 940) { return "Ouest";};
    if(localweathercock > 890 and localweathercock < 930) { return "Nord-Ouest";};
    if(localweathercock > 790 and localweathercock < 830) { return "Nord";};
    if(localweathercock > 470 and localweathercock < 500) { return "Nord-Est";};
    if(localweathercock > 80 and localweathercock < 120) { return "Est";};
    if(localweathercock > 180 and localweathercock < 220) { return "Sud-Est";};
    if(localweathercock > 290 and localweathercock < 320) { return "Sud";};  
    if(localweathercock > 640 and localweathercock < 670) { return "Sud-Ouest";};  
  }
}

// Calculer la vitesse de rotation de l'anémomètre
int getAnemometerRotate(int anenometer_time) {
  if (ANEMOMETER_sensor) {
    return getSensorAnemometer() * (60 * ANEMOMETER_MEASUREMENT_INTERVAL_GUST / PULSES_PER_TURN ) / anenometer_time;
  }
}

// Calcule la vitesse du vent dynamique de l'anémomètre
float getAnemometerGust(int anenometer_time) {
  int anemometer_rotate = getAnemometerRotate(anenometer_time);
  return 2.0 * 3.1415926 * transferFunction(anemometer_rotate) * ANEMOMETER_RAYON * anemometer_rotate;
}

// Obetenir la vitesse de rotation de l'anémomètre
int getLocalAnemometerRotate() {
  Serial.println("Relève la vitesse de rotation de l'anémomètre");
  return localanemometerrpm;
}

// Obtenir la vitesse du vent dynamique de l'anémomètre
float getLocalAnemometerGust() {
  Serial.println("Relève la vitesse du vent actuelle");
  return localanemometergust;
}

// Obtenir la vitesse moyenne du vent
float getLocalAnemometerSpeed() {
  Serial.println("Relève la vitesse moyenne du vent");
  return localanemometerspeed;
}

// Fait une mesure des capteurs de la station météo
void ICACHE_RAM_ATTR setSensorsMeasure() {
  interrupts++;
  getsensorsvalues =true;  
}

// Relève les valeurs de tous les capteurs
void getValuesSensors() {
  String textlocaltemp;
  String textlocalhumidity;
  String textlocalpressure;
  String textlocallux;
  String textlocalrain;
  String textlocalrainlevel;
  String textlocalweathercock;
  String textlocalwindgust;
  String textlocalwindspeed;
  String jsonBuffer;
  
  if(WiFi.status()== WL_CONNECTED){
    Serial.println("Relève la météo internet");
    WiFiClient client;
    HTTPClient http;
    String serverName = "http://api.openweathermap.org/data/2.5/weather";
    String serverPath = serverName + "?id=" + OPEN_WEATHER_MAP_LOCATION_ID + "&lang=" + OPEN_WEATHER_MAP_LANGUAGE + "&units=" + (IS_METRIC ? "metric" : "imperial") + "&APPID=" + OPEN_WEATHER_MAP_APP_ID;
    //Serial.println(serverPath);
    http.begin(client, serverPath.c_str());
    int httpCode = http.GET();

    if (httpCode > 0) {
      Serial.println("Code de sortie HTTP " + String(httpCode));
      jsonBuffer = http.getString();
      Serial.println(jsonBuffer);
      deserializeJson(openWeatherMap, jsonBuffer);
      textlocaltemp = openWeatherMap["main"]["temp"].as<String>();
      textlocalhumidity = openWeatherMap["main"]["humidity"].as<String>();
      textlocalpressure = openWeatherMap["main"]["pressure"].as<String>();
      //textlocallux = openWeatherMap;
      textlocalrain = openWeatherMap["rain"]["rain.1h"].as<String>();
      //textlocalrainlevel = openWeatherMap["rain"]["rain.3h"].as<String>();
      textlocalweathercock = openWeatherMap["wind"]["deg"].as<String>();
      textlocalwindgust = openWeatherMap["wind"]["gust"].as<String>();
      textlocalwindspeed = openWeatherMap["wind"]["speed"].as<String>();
    } else {
      Serial.println("Échec de la connexion HTTP");
    }
  }

  // Lecture de la température
  if(temperature_sensor){
    localtemp = getLocalTemperature();
    show_temperature = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localtemp = textlocaltemp.toFloat();
    show_temperature = true;
  } else {
    show_temperature = false;
  }
  
  // Lecture de l'humidité
  if(humidity_sensor) {
    localhumidity = getLocalHumidity();
    show_humidity = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localhumidity = textlocalhumidity.toFloat();
    show_humidity = true;
  }
  
  // Lecture de la pression atmosphérique
  if(pressure_sensor) {
    Serial.println("Relève la pression actuelle");
    localpressure = getLocalPressure();
    show_pressure = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localpressure = textlocalpressure.toFloat();
    show_pressure = true;
  }
  
  // Lecture de la luminosité
  if(light_sensor) {
    Serial.println("Relève la luminosité actuelle");
    locallux = getLocalLight();
  }
  
  // Lecture de la dectection de pluie ou de neige
  if(rain_sensor) {
    Serial.println("Relève l'état de précipitation actuelle");
    localrain = getLocalRain();
    show_rain = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localrain = textlocalrain.toFloat();
    show_rain = true;
  }
  
  // Lecture du niveau de pluie journalié
  if(rainlevel_sensor) {
    Serial.println("Relève le niveau de précipitation actuelle");
    localrainlevel = getLocalRainLevel();
  }

  // Lecture de l'orientation du vent
  if(weathercock_sensor) {
    Serial.println("Relève l'orientation du vent actuelle");
    localweathercock = getSensorWeathercock();
    localweathercocktext = getLocalWeathercock();
    show_weathercock = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localweathercock = textlocalweathercock.toFloat();
    localweathercocktext = getLocalWeathercock();
    show_weathercock = true;
  }

  // Obtension des valeurs des capteurs de la station météo
  if (RAINLEVEL_sensor) {
    rainGaugeSave();
  }

  // Lecture de la vitesse du vent
  if(anemometer_sensor) {
    getLocalAnemometerGust(); 
    show_anemometer = true;
  } else if (WiFi.status() == WL_CONNECTED) {
    localanemometergust = textlocalwindgust.toFloat();
    show_anemometer = true;
  }
}

String getDisplayTemperature() {
  if (show_temperature) {
    return String(localtemp, 1) + (IS_METRIC ? "°C" : "°F");
  } else {
    return "None";
  }
}

String getDisplayHumidity() {
  if (show_humidity) {
    return String(localhumidity, 0) + "%";
  } else {
    return "None";
  }
}

String getDisplayPressure() {
  if (show_pressure) {
    return String(localpressure, 0) + " hPa";
  } else {
    return "None";
  }
}

String getDisplayRainlevel() {
  if (show_rainlevel) {
    return String(localrainlevel, 1) + "mm³";
  } else {
    return "None";
  }
}

String getDisplayWeathercock() {
  if (show_weathercock) {
    return localweathercocktext;
  } else {
    return "None";
  }
}

String getDisplayAnemometerGust() {
  if (show_anemometer) {
    return String(localanemometergust, 0) + "m/s";
  } else {
    return "None";
  }
}
