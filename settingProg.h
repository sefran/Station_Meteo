bool getsensorsvalues = true;
int getmesurementsensors;
byte number_of_sensor;
bool oled_display = false; // autodetect
bool temperature_sensor = false; // autodetect DHT22, AHT10, BME280
bool humidity_sensor = false; //DHT22 but autodetect  SHTC3, AHT10
bool humidity_ground_sensor = false;
bool pressure_sensor = false; // autodetect BME280
bool light_sensor = false; // autodetect BH1750VFI
bool rain_sensor = false;
bool raindrops_sensor = false;
bool rainlevel_sensor = false;
bool weathercock_sensor = false;
bool anemometer_sensor = false;
int getmesurementanemometer;
bool altitude_sensor = false; // autodetect BME280

// Timer
void setSensorsMeasure();

// Initialise l'affichage oled pour l'adresse 0x3c
// sda-pin=D2=11 et sdc-pin=D1=10
SSD1306Wire     display(I2C_DISPLAY_ADDRESS, SDA_PIN, SDC_PIN);
OLEDDisplayUi   ui( &display );
int remainingTimeBudget;

// Configuration du capteur DHT11/DHT22 Humidité, température
bool DHT_sensor = false;
DHT dht(DHT_PIN, DHT_TYPE);

// *** Configuration capteurs I2C
// Configuration du capteur SHTC3 Humidité, température I2C
bool SHTC3_sensor = false;
bool shtc3_i2c = false;
//SHTC3 shtc3;
ClosedCube::Sensor::SHTC3 shtc3;
ClosedCube::Sensor::SHTC3_Result result;
// Configuration du capteur AHT10 Humidité, température I2C
bool AHT10_sensor = false;
bool aht10_i2c = false;
Adafruit_AHT10 aht;
Adafruit_Sensor *aht_humidity, *aht_temp;
sensors_event_t ahttemperature;
sensors_event_t ahthumidity;
// Configuration du capteur BME280 Pression, température I2C
bool BME280_sensor = false;
bool BME280_i2c = false;
BME280 bme280;
char pressure_value[4];
// Configuration du capteur BMP180 Pression, température I2C
bool BMP180_sensor = false;
bool BMP180_i2c = false;
Adafruit_BMP085 bmp180;
// Configuration du capteur de lumière BH1750FVI GY-30 I2C
bool BH1750FVI_sensor = false;
bool bh1750fvi_i2c = false;
BH1750FVI lightSensor(I2C_BH1750FVI_ADDRESS, SDA_PIN, SDC_PIN);
// Capteur ADS1115 avec 4 entrées CAN 16 bits
bool ADS1115_sensor = false;
bool ads1115_i2c = false;
ADS1115 adscan(I2C_ADS1115_ADDRESS);

// Configuration du capteur de pluie
bool RAIN_sensor = false;

// Configuration du capteur de gouttes de pluie
bool RAINDROPS_sensor = false;

// Configuration Pluviomètree
bool RAINLEVEL_sensor = false;
volatile byte rainlevel_pulses;
void handleCountRainLevelPulses();

// Configuration Girouette
bool WEATHERCOCK_sensor = false;
const int ESP8266_WEATHERCOCK_PIN = A0; // Lecture de l'orientattion sur l'entrée analogique de l'ESP8266

// Configuration Anémomètre
bool ANEMOMETER_sensor = false;
float gustadd;
float gustnumber;
float anemometer_timevalue;
unsigned long anemometer_timeold;
// Déclaration fonction à cause de ICACHE_RAM_ATTR pour interuptions
void handleCountPulses();

// Configuration altimètre
bool ALTITUDE_sensor = false;

//
// Configurations logicielles
//
// Variables de mesure des capteurs
volatile byte localanemometerpulses;
unsigned int localanemometerrpm;
float localanemometergust;
float localanemometerspeed;
float localhumidity;
float localtemp;
float localpressure;
float locallux;
int localrain;
bool localraindrop;
float localrainlevel;
int localweathercock;
String localweathercocktext;

bool show_temperature = false;
bool show_humidity = false;
bool show_humidity_ground = false;
bool show_pressure = false;
bool show_light = false;
bool show_rain = false;
bool show_raindrops = false;
bool show_rainlevel = false;
bool show_weathercock = false;
bool show_anemometer = false;
bool show_altitude = false;

Ticker timer;
volatile int interrupts;

time_t now;
time_t localnow;
bool flagheure = true;
long timeSinceLastSensorsUpdate = 0;

DynamicJsonDocument  openWeatherMap(1024);

// Forecast
// drapeau changé dans la fonction ticker toutes les 10 minutes
bool readyForWeatherUpdate = false;
String lastUpdate = "--";
long timeSinceLastWUpdate = 0;

// déclaration des fonctions pour l'affichage
void drawProgress(OLEDDisplay *display, int percentage, String label);
void updateData(OLEDDisplay *display);
void drawDateTime(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawCurrentWeather(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawForecast(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);
void drawForecastDetails(OLEDDisplay *display, int x, int y, int dayIndex);
void drawHeaderOverlay(OLEDDisplay *display, OLEDDisplayUiState* state);
void setReadyForWeatherUpdate();
void drawLocalWeather(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y);

OpenWeatherMapCurrentData currentWeather;
OpenWeatherMapCurrent currentWeatherClient;
OpenWeatherMapForecastData forecasts[MAX_FORECASTS];
OpenWeatherMapForecast forecastClient;

// Ajouter des cadres
// ce tableau conserve les pointeurs de fonction vers toutes les affichages
// les cadres sont des vues uniques qui glissent de droite à gauche
// Cadres défillants
FrameCallback framestime[] = { drawDateTime };
FrameCallback frameslocal[] = { drawDateTime, drawLocalWeather };
FrameCallback framesnet[] = { drawDateTime, drawCurrentWeather, drawForecast };
FrameCallback framesall[] = { drawDateTime, drawCurrentWeather, drawLocalWeather, drawForecast };
int numberOfFrames;

// Bandeau statique
OverlayCallback overlays[] = { drawHeaderOverlay };
int numberOfOverlays = 1;

ESP8266WebServer server(80);
