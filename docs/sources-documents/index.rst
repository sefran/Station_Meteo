.. |date| date::

:Date: |date|
:Revision: |version|
:Author: FrancSERRES <sefran007@gmail.com>
:Description: Documentation Station Météo ESP8266
:Info: Voir <https://sefran.frama.io/Station_Meteo/> pour la mise à jour de cette documentation.

.. toctree::
   :maxdepth: 3
   :caption: Contenu

.. include:: StationMétéoESP8266.rst
