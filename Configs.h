// --------------------------------
// Début Configurations Matérielles
// --------------------------------
const String dht_connect = "0"; //Options : esp8266, pcf8574t, 0
const String humidity_ground_connect = "0"; // esp8266, ads1115, 0
const String rain_connect = "0"; // esp8266, ads1115, 0
const String rain_drops_connect = "0"; // esp8266, ads1115, 0
const String rainlevel_connect = "0"; // esp8266, ads1115, 0
const String weathercock_connect = "0"; // esp8266, ads1115, 0
const String anemometer_connect = "0"; // esp8266, ads1115, 0

// LED de l'ESP8266 pour l'accès WEB
const int LED_PIN = 13;

// Temps de relevé des capteurs
const int UPDATE_SENSORS = 1 * 1000; // Toutes les secondes (secondes * millisecondes)

// Configuration I2C
const int SDA_PIN = D3;
const int SDC_PIN = D4;
#define USER_PIN false
#define DISPLAY_ERROR false 
#define LOOP_DELAY    10000
// --- Configuration adresses I2C ---
// Adresse I2C de l'affichage
const int I2C_DISPLAY_ADDRESS = 0x3c;
// Adresse I2C du capteur de lumière BH1750FVI
const int I2C_BH1750FVI_ADDRESS = 0x23;
// Adresse I2C du capteur SHTC3 Température et Humidité
const int I2C_SHTC3_ADDRESS = 0x70;
// Adresse I2C du capteur AHT10 Température et Humidité
const int I2C_AHT10_ADDRESS = 0x38;
// Adresse I2C du capteur BME280 Température, Humidité et Pression
const int I2C_BME280_ADDRESS = 0x76;
// Adresse I2C du capteur BMP180 Température et Pression
const int I2C_BMP180_ADDRESS = 0x77;
// Adresse I2C de l'ADS1115 (avec 4 entrées CAN 16 bits)
const int I2C_ADS1115_ADDRESS = 0x48;

// Configuration du capteur DHT11/DHT22 Humidité, température
#define DHT_PIN D5
#define DHT_TYPE DHT11

// Configuration du Capteur de pluie
// Configurations avec carte ADS1115
byte rain_sensor_ads1115_adc = 1;

// Configuration du Capteur de goutes/pluie
#define RAIN_DROPS_PIN D5
// Configurations avec carte ADS1115
byte rain_drops_ads1115_adc = 2;

// Configuration du capteur d'humidité de sol
#define HG_PIN D6
// Configurations avec carte ADS1115
byte humidity_ground_ads1115_adc = 3;

// Configuration du capteur de lumière BH1750FVI
int DAY_LIGHT_VALUE = 100;

// Configuration du capteur BME280 Pression, température I2C
#define SEALEVELPRESSURE_HPA (1009.5)

// Configuration Pluviomètree
bool LEXCA009_sensor = false;
int LEXCA009_PIN = D1; // Capteur LEXCA009
float RAINLEVEL_VOLUME = 0.2794; // Volume du capteur LEXCA009
int RAINLEVEL_PULSE_OFFSET = 2; // Offset du capteur de basculement du LEXCA009

// Configuration Girouette LEXCA003
bool LEXCA003_sensor = false;
// Configurations avec carte ADS1115
byte weathercock_ads1115_adc = 0; // Configurations avec carte ADS1115

// Configuration Anémomètre LEXCA002
bool LEXCA002_sensor = false;
int LEXCA002_PIN = D2; // connecteur du LEXCA002
const unsigned int PULSES_PER_TURN = 1; // Nombre de pulsations pour un tour de l'anémomètre
const float ANEMOMETER_RAYON = 0.07; // Rayon en m
// Configuration de la mise à jours des données des capteurs
const unsigned int ANEMOMETER_MEASUREMENT_INTERVAL_GUST = 5; // Intervalle de mesure de la vitesse du vent 25s
const unsigned int BASE_TIME_MESUREMENT_ANEMOMETER = 1; // valeur 1 -> 1s, 60 -> 1min, 3600 -> 1h
const unsigned int ANEMOMETER_MEASUREMENT_INTERVAL_SPEED = 300000; // Intervalle de mesure de la vitesse du vent 5 min
// ------------------------------
// Fin Configurations Matérielles
// ------------------------------


// --------------------------------
// Début Configurations logicielles
// --------------------------------
#define LOCAL_STATE        "France"
#define LOCAL_CITY         "Frontignan"

// Les unités sont-elle métriques ou anglo-saxonnes ?
const boolean IS_METRIC = true;

// Configuration du serveur de temps
const char* ntpServer = "fr.pool.ntp.org";
// Ajustez selon votre langue
const String WDAY_NAMES[] = {"DIM", "LUN", "MAR", "MER", "JEU", "VEN", "SAM"};
const String MONTH_NAMES[] = {"JAN", "FEV", "MAR", "AVR", "MAI", "JUN", "JUL", "AOU", "SEP", "OCT", "NOV", "DEC"};

// Configuration de OpenWeatherMap
//Allez sur https://openweathermap.org/find?q= pour trouver votre localisation météo.
//Sélectionnez l'entrée la plus proche de l'emplacement réel que vous souhaitez afficher.
//Ce sera une URL du genre https://openweathermap.org/city/2988507.
//Le nombre à la fin du lien est ce que vous attribuez comme localité météo à la constante ci-dessous.
String OPEN_WEATHER_MAP_LOCATION_ID = "3016956";

// Choisissez un code de langue dans la liste ci-dessous:
/* Arabe - ar, Bulgare - bg, Catalan - ca, Tchèque - cz, Allemand - de, Grec - el,
   Anglais - en, Persan (farsi) - fa, Finlandais - fi, Français - fr, Galicien - gl,
   Croate - hr, Hongrois - hu, Italien - it, Japonais - ja, Coréen - kr,
   Letton - la, Lituanien - lt, Macédonien - mk, Néerlandais - nl, Polonais - pl,
   Portugais - pt, Roumain - ro, Russe - ru, Suédois - se, Slovaque - sk,
   Slovène - sl, Espagnol - es, Turc - tr, Ukrainien - ua, Vietnamien - vi,
   Chinois simplifié - zh_cn, Chinois Traditionnel - zh_tw.
*/
String OPEN_WEATHER_MAP_LANGUAGE = "fr";

// Nombre de variables déclarées dans Forecast
const uint8_t MAX_FORECASTS = 4;

// Configuration de la mise à jours des données OpenWeatherMap
const int UPDATE_INTERVAL_SECS = 10 * 60; // Toutes les 10 minutes (min * secondes)

// Configuration de la mise à jours des données des capteurs
const int MESUREMENT_SENSORS_INTERVAL = 20; // (min 1)
const int BASE_TIME_MESUREMENT_SENSOR = 1; // valeur 1 -> 1s, 60 -> 1min, 3600 -> 1h

// Afficheur
#define DISPLAY_FPS       30 // Vitesse de rafraichissement de l'afficheur
#define DISPLAY_DIRECTION LEFT_RIGHT // Définit l'emplacement du premier cadre dans la barre.
// Position de l'indicateur de tableau d'affichage
// Vous pouvez changer cela en
/* TOP=Haut, LEFT=Gauche, BOTTOM=Bas, RIGHT=Droite
*/
#define DISPLAY_POSITION  BOTTOM 
// Vous pouvez modifier la transition utilisée
/* SLIDE_LEFT=Défile sur la gauche, SLIDE_RIGHT=Défile sur la droite,
   SLIDE_UP=Défile vers le haut, SLIDE_DOWN=Défile vers le bas
*/
#define DISPLAY_ANIMATION SLIDE_LEFT
// ------------------------------
// Fin Configurations logicielles
// ------------------------------
 
