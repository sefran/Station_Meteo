// Configuration du WIFI
const char* WIFI_SSID = "votre-nom-wifi-ssid";
const char* WIFI_PWD = "votre-mot-de-passe-wifi";

// Configuration de OpenWeatherMap
// Connectez-vous sur ce site pour lire comment obtenir une clé API:
// https://docs.thingpulse.com/how-tos/openweathermap-key/
String OPEN_WEATHER_MAP_APP_ID = "votre-clé-openweathermap";
