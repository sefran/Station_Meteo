# [Station Météo ESP8266](https://sefran.frama.io/Station_Meteo/)

# Projet

Mettre en œuvre une station météo locale i2c complète, avec des prévisions météos, afin de l’utiliser avec une VMC Thermodynamique (que je vais numériser). Elle récupérera des prévisions météos sur le site OpenWeather suivant le lieu de pose de la station météo.

Les capteurs connectés à cette station météo seront modulaires et leurs détections se fera automatiquement par l’I2C.



![image](docs/sources-documents/images/StationMeteo.jpg)

## Le setup



![image](docs/sources-documents/images/setup.png)


* initHardware() :

    Configure la diode électroluminescente d’accès WEB, active l’I2C avec **initI2C()**, initialise l’écran OLED si présent avec **detectOLEDDisplay()** et affiche le logo de démarrage sur l’écran OLED.


* initNetwork() :

    Active le wifi avec **wifiConnect()**, active le DNS avec **startmDNS()**.


* syncNTP() :

    Synchronise la référence de temps NTP (fonction dans initNetwork).


* scanI2C() :

    Détecte les capteurs I2C de la station météo.


* initLoopTime() :

    Paramètre la référence de temps horaire pour la station météo (fonction dans initTime)

## Une GUI sur un afficheur OLED est possible

Une API GUI de base pour gérer l’affichage qui se trouve dans **initDisplay**.


* clearDisplay() :

    Pour effacer l’écran.


* drawTitle() :

    Pour afficher un titre à une position donnée.


* drawMessage() :

    Pour afficher un texte à une position donnée.


* drawCounter() :

    Pour afficher une activité à une position donnée.


* drawProgress() :

    Pour afficher une progression à une position donnée.


* drawLogo() :

    Pour afficher un logo à une position donnée.

## Des affichages pour gérer les différentes informations de la station météo

Des affichages système qui se trouve dans **initDisplay**.


* infoInitScreen() :

    Pour afficher l’initialisation de l’OLED.


* infoProcess() :

    Pour afficher l’état des processus en cours.


* infoProgress() :

    Pour afficher la progression de détection des capteurs et fonctions météorologiques.



![image](docs/sources-documents/images/InfoSystem.png)

Des affichages d’informations de la station météorologique qui se trouve dans **initDisplay**.


* infoTime() :

    Pour afficher la date, l’heure et le lieu de la station.


* infoNetworkWeather() :

    Information internet synthétique de la météo locale (prévision journalière d’OpenWeather).


* infoNetworkWeatherAll() :

    Toutes les informations internet de la météo locale.


* infoNetworkForecast() :

    Informations prévisionnelles internet synthétiques sur 3 jours de la météo locale (OpenWeather).


* infoTempérature() :

    Informations de température de la station météo.


* infoHumidity() :

    Informations d’humidités et de pluie de la station météo.


* infoPressure() :

    Informations de pressions et d’altitude de la station météo.


* infoWind() :

    Informations de force du vent et de son orientation de la station météo.


* infoLight() :

    Informations de luminosité, de lever et coucher de soleil, de jour/huit de la station météo.



![image](docs/sources-documents/images/InfoMétéo.png)



![image](docs/sources-documents/images/Site_Web_Mesures_Locales.png)

Un serveur WEB donnera aussi une synthèse de la météo.

Le site Web [http://ip_station_météo](http://ip_station_météo)

Tous les capteurs seront accessibles par le réseau au travers d’une API WEB façon RESTful.


* DHT11


    * /DHT : Présence du capteur.


    * /DHT/temperature : Valeur de la température.


    * /DHT/humidity : Valeur du taux d’humidité en %.


* SHTC3


    * /SHTC3 : Présence du capteur


    * /SHTC3/temperature : Valeur de la température.


    * /SHTC3/humidity : Valeur du taux d’humidité en %.


* AHT10


    * /AHT10 : Présence du capteur.


    * /AHT10/temperature : Valeur de la température.


    * /AHT10/humidity :  Valeur du taux d’humidité en %.


* BME280


    * /BME280 : Présence du capteur.


    * /BME280/temperature : Valeur de la température.


    * /BME280/pressure : Valeur de la pression barométrique.


* BH1750


    * /BH1750 : Présence du capteur.


    * /BH1750/light : Valeur de l’éclairage.


* Densité de pluie


    * /rain : Présence du capteur.


    * /rain/value : Valeur de la densité du pluie.


    * /rain/text : Interprétation textuelle de la densité de pluie.


* Niveau de pluie


    * /rainlevel : Présence du capteur.


    * /rainlevel/value : Niveau de pluie de la journée.


    * /rainlevel/week : Niveaux de pluie de la semaine.


    * /rainlevel/month : Niveaux de pluie du mois.


    * /rainlevel/year : Niveaux de pluie de l’année.


* Girouette


    * /weathercock : Présence du capteur.


    * /weathercock/rotate : Indicateur numérique de position.


    * /weathercock/direction : Indication cardinale de la girouette.


* Anémomètre


    * /anemometer : Présence du capteur.


    * /anemometer/gust : Vitesse de rafale du vent.


    * /anemometer/speed : Vitesse moyenne du vent.

# Matériel

Un esp8266 NodeMcu 1.0 et un NodeMcu breadboard.



![image](docs/sources-documents/images/ESP8266.jpg)

Un ecran OLED Bleu jaune de 0.96 pouces.



![image](docs/sources-documents/images/OLED-BleuJaune0.96.jpg)

Un capteur DHT11 pour mesurer le niveau d’humidité de l’air et la température.



![image](docs/sources-documents/images/DHT11.jpg)

Un capteur AHT10 pour mesurer la température et l’humidité.



![image](docs/sources-documents/images/AHT10.jpg)

Un capteur SHTC3 pour mesurer la température et l’humidité.



![image](docs/sources-documents/images/SHTC3.jpg)

Un capteur BME/BMP280 pour mesurer la température, l’humidité et avoir le baromètre et la pression de l’air.



![image](docs/sources-documents/images/BME280.jpg)

Un capteur BMP180 pour mesurer la température et avoir le baromètre et la pression de l’air.



![image](docs/sources-documents/images/BMP180.jpg)

Un capteur de lumière BH1750FVI pour déterminer le jours et la nuit, ainsi que le temps couvert ou ensoleillé.



![image](docs/sources-documents/images/BH1750FVI.jpg)

Un capteur densité d’eau de pluie pour déterminer les précipitations de pluie ou de neige.



![image](docs/sources-documents/images/CapteurPluie.jpg)

Un capteur de goûtes et densité d’eau de pluie pour déterminer les précipitations de pluie ou de neige.



![image](docs/sources-documents/images/CapteurPluie2.jpg)

Un capteur d’humidité du sol”.



![image](docs/sources-documents/images/CapteurHumiditéSol.jpg)

Un pluviomètre LEXCA009 pour déterminer le niveau de pluie journalier.



![image](docs/sources-documents/images/Pluviomètre.jpg)

Une girouette LEXCA003 pour déterminer l’orientation du vent.



![image](docs/sources-documents/images/Girouette.jpg)

Un anémomètre LEXCA002 pour déterminer rafales et vitesse du vent.



![image](docs/sources-documents/images/Anémomètre.jpg)

Une platine pour connecter le pluviomètre, la girouette et l’anémomètre.



![image](docs/sources-documents/images/PlatinePGA.jpg)

Un convertisseur I2C ADS1115 16bit Analogique Numérique en 4 canaux pour convertir en I2C la mesure de la girouette LEXCA003.



![image](docs/sources-documents/images/ADS1115.jpg)

Une carte d’extension de ports I2C pour convertir L’anémomètre LEXCA002, le pluviomètre LEXCA001, les capteurs de densité de pluie, de goûtes et d’humidité du sol en interface I2C



![image](docs/sources-documents/images/PCF8574T.jpg)

# Logiciel

Travaillez vos icônes SVG avec inkscape et définissez la hauteur de vos images en 28 pixels.

Fabrication des icônes xbm pour votre afficheur à partir du SVG :

```console
mogrify -path ./xbm -format xbm *.svg
```

Créer un fichier .h dans le répertoire **/fonts** du projet contenant votre jeux d’icônes.

Définir une structure d’icône.

```bash
// ** Icon list **
struct structiconlist {
    char description[50];
    uint8_t  width;
    uint8_t  height;
    unsigned char pixmap[100];
};
```

Copier le contenu des fichiers .xbm d’icônes dans ce fichier.

```bash
#define weather_pictogram_alien_width 24
#define weather_pictogram_alien_height 28
static char weather_pictogram_alien_bits[] = {
   0x00, 0x00, 0x00, 0x80, 0xFF, 0x01, 0xE0, 0xFF, 0x03, 0xF0, 0xFF, 0x0F,
   0xF8, 0xFF, 0x1F, 0xFC, 0xFF, 0x3F, 0xFC, 0xFF, 0x3F, 0xFE, 0xFF, 0x7F,
   0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F,
   0xC2, 0xFF, 0x43, 0x02, 0xFF, 0x40, 0x02, 0x7E, 0x60, 0x06, 0x3C, 0x20,
   0x04, 0x3C, 0x20, 0x08, 0x18, 0x10, 0x18, 0x18, 0x18, 0x30, 0x18, 0x0C,
   0xF0, 0xDB, 0x0F, 0xE0, 0xFF, 0x07, 0xC0, 0xFF, 0x03, 0x80, 0xFF, 0x01,
   0x00, 0xFF, 0x00, 0x00, 0x7E, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x00,
};
```

Et le modifier ainsi en précisant le nom de l’icône dans la langue voulue, la taille de référence de l’icône, le pixmap, et en ajoutant un positionnement en EPROM avec **PROGMEM** de la structure.

```bash
const structiconlist icon_alien PROGMEM = {
   // description as string, with as unsigned uint8_t, height as unsigned uint8_t, pixmap icon datas 28x28
   // - clear sky
   "extraterrestre", 24, 28,
   {
        0x00, 0x00, 0x00, 0x80, 0xFF, 0x01, 0xE0, 0xFF, 0x03, 0xF0, 0xFF, 0x0F,
        0xF8, 0xFF, 0x1F, 0xFC, 0xFF, 0x3F, 0xFC, 0xFF, 0x3F, 0xFE, 0xFF, 0x7F,
        0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F, 0xFE, 0xFF, 0x7F,
        0xC2, 0xFF, 0x43, 0x02, 0xFF, 0x40, 0x02, 0x7E, 0x60, 0x06, 0x3C, 0x20,
        0x04, 0x3C, 0x20, 0x08, 0x18, 0x10, 0x18, 0x18, 0x18, 0x30, 0x18, 0x0C,
        0xF0, 0xDB, 0x0F, 0xE0, 0xFF, 0x07, 0xC0, 0xFF, 0x03, 0x80, 0xFF, 0x01,
        0x00, 0xFF, 0x00, 0x00, 0x7E, 0x00, 0x00, 0x3C, 0x00, 0x00, 0x00, 0x00,
   }
};
```

Vous pouvez alors appeler l’icône par :

```bash
afficheur.drawXbm(position_x, position_y, icon_alien.width, icon_alien.height, icon_alien.pixmap);
```

# La station météo en fonctionnement

[Voir sur Youtube](https://youtu.be/tvuLMToEEQQ)
