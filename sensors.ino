float getDHTxxTemperature() {
  if(IS_METRIC) {
    return dht.readTemperature();
  } else {
    return dht.readTemperature(true);
  }
}

float getDHTxxHumidity() {
  return dht.readHumidity();
}

float getSHTC3Temperature() {
  shtc3.wakeUp();
  result = shtc3.readTempAndHumidity();
  shtc3.sleep();
  if(IS_METRIC) {
    return result.t;
    //return shtc3.toDegC();
  } else {
    return (result.t*9.0/5.0)+32.0;
    // return shtc3.toDegF();
  }
  
}
float getSHTC3Humidity() {
  shtc3.wakeUp();
  result = shtc3.readTempAndHumidity();
  shtc3.sleep();
  return result.rh;
  //return shtc3.toPercent();
}

float getAHT10Temperature() {
  aht_temp->getEvent(&ahttemperature);
  if(IS_METRIC) {
    return ahttemperature.temperature;
  } else {
    return (ahttemperature.temperature*9.0/5.0)+32.0;
  }
}

float getAHT10Humidity() {
  aht_humidity->getEvent(&ahthumidity);
  return ahthumidity.relative_humidity;
}

float getBME280Temperature() {
  if(IS_METRIC) {
    return  bme280.readTempC();
  } else {
    return  bme280.readTempF();
  }
}

float getBME280Humidity() {
  return bme280.readFloatHumidity();
}

float getBME280Pressure() {
  return bme280.readFloatPressure() / 100.0;
}

float getBME280Altitude() {
  if(IS_METRIC) {
    return bme280.readFloatAltitudeMeters();
  } else {
    return bme280.readFloatAltitudeFeet();
  }
}

float getBME280DewPoint() {
  if(IS_METRIC) {
    return bme280.dewPointC();
  } else {
    return bme280.dewPointF();
  }  
}

float getBMP180Temperature() {
  if(IS_METRIC) {
    return  bmp180.readTemperature();
  } else {
    return  (bmp180.readTemperature() * 9.0 / 5.0) + 32.0;
  }
}

float getBMP180Pressure() {
  return bmp180.readPressure() / 100.0;
}

float getBMP180Altitude() {
  if(IS_METRIC) {
    return bmp180.readAltitude();
  } else {
    return bmp180.readAltitude() * 3.28084;
  }
}

float getBH1750Light() {
  return lightSensor.getLux();
}

int getSensorRain() {
  if (rain_connect.equals("esp8266")) {
    return analogRead(A0);
  } else if (rain_connect.equals("ads1115")) {
    //return ;
  }
}

bool getSensorDrops() {
  //return ;
}

int getHumidityGround() {
 // return ;
}

// Pour qu'une interruption extérieure s'exécute il faut que la fonction soit en mémoire et nom en flash donc ICACHE_RAM_ATTR
void ICACHE_RAM_ATTR handleCountRainLevelPulses() {
  Serial.println("Détection d'une bascule du niveau de pluie");
  rainlevel_pulses++;
}

float getSensorRainLevel() {
  return rainlevel_pulses;
}

int getSensorWeathercock() {
  if (weathercock_connect.equals("esp8266")) {
    return analogRead(ESP8266_WEATHERCOCK_PIN);
  } else if (weathercock_connect.equals("ads1115")) {
    //return ;
  } else {
    //return ;
  }
}

// Pour qu'une interruption extérieure s'exécute il faut que la fonction soit en mémoire et nom en flash donc ICACHE_RAM_ATTR
void ICACHE_RAM_ATTR handleCountPulses() {
  Serial.println("Détection d'une impulsion anémomètre");
  localanemometerpulses++;
}

int getSensorAnemometer() {
  return localanemometerpulses;
}
