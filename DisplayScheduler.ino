void drawDateTime(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  display->setTextAlignment(TEXT_ALIGN_CENTER);

  // Affiche la date
  display->setFont(ArialMT_Plain_10);
  display->drawString(64 + x, 5 + y, getDate());

  // Affiche l'heure
  display->setFont(ArialMT_Plain_24);
  display->drawString(64 + x, 15 + y, getHeure(3));
  display->setTextAlignment(TEXT_ALIGN_LEFT);
}
